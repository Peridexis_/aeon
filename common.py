from typing import Sequence, Union

class Shorthands:
	"""A class to store a dict of shorthands, and to expand a name with them."""
	def __init__(self, data=dict()):
		self._data = data

	def expand(self, name: str, skill: str =None) -> list:
		"""Returns a list of all possible versions of the str when expanded

		For example, if the shorthands config has an entry that was
		"DIRECTIONS":["Up", "Right", "Down", "Left"], and this was
		passed an input str of "DIRECTIONdog", then the output would be
		["Updog", "Rightdog", "Downdog", "Leftdog"]. Multiple replacements
		will stack, so if "FIRSTNAME" had 5 options, and "LASTNAME" had 3,
		then "FIRSTNAME LASTNAME" would result in a list 15 entries long.

		This will use the Shorthands config dict, as well as using the list
		of weapons from the weapons config file for "WEAPON", using the
		list of weapon skills from the same file for "WSKILL", and possibly
		using the given skill name to replace instances of "THIS", if given.
		Lastly, the magical elements are "PUREELEMENT", "PRIMEELEMENT", and 
		"ANYELEMENT".

		Args:
			name (str): The name to expand into a list of possibilities
			skill (str): Optional skill name to replace "THIS" with
		Returns:
			A list of possible versions of "name" with shorthands replaced
		"""
		expand = [name]
		sh = self._data
		if skill is not None:
			sh["THIS"] = [skill]

		for key, repl in sh.items():
			if key not in name: continue
			expand = [e.replace(key, r) for e in expand for r in repl]
		return expand

class PreqNode(list):
	"""A node for building a prerequisite tree. Subclass of list.

	The operator determines what is returned when checking if a list
	of skills meets a prerequisite tree. An AND is true if all chid 
	nodes are true, OR if any are, XOR if an odd number are. NAND, 
	NOR, and XNOR are the logical inversions of those. ALL, ANY, ODD,
	SOME, NONE, and EVEN are the same as the six gate-named ones, 
	there fore usability. TRUE and FALSE always return those values.
	Lastly, LEAF is used for the final nodes on the tree, and 
	contains the skill info that a skill list will be checked against.

	Attributes:
		operator (str): The operator used to evaluate a node
		name (str): The name on a leaf node to compare with a skill
		level (str): The level on a leaf node to compare with a skill
		tagged (str): Whether the skills' tags must match to meet this prereq
	"""
	_opperator_map = {
		"AND":  all, # Every subtree must be satisfied
		"OR":   any, # At least one subtree must match
		"XOR":  lambda s: bool(sum(s) % 2), # Odd number of matches
		"NAND": lambda s: not all(s), # At least one non-match
		"NOR":  lambda s: not any(s), # No subtree can match
		"XNOR": lambda s: not (sum(s) % 2), # Even number of matches
		"ALL":  all, # Same as AND
		"ANY":  any, # Same as OR
		"ODD":  lambda s: bool(sum(s) % 2), # Same as XOR
		"SOME": lambda s: not all(s), # Same as NAND
		"NONE": lambda s: not any(s), # Same as NOR
		"EVEN": lambda s: not (sum(s) % 2), # Same as XNOR
		"TRUE": lambda s: True, # Always true
		"FALSE":lambda s: False, # Always false
		"ROOT": all # Root is just an AND that displays more cleanly
	}

	def __init__(
			self, 
			data: Sequence, 
			operator: str = "AND", 
			skill: str = '', 
			sh: Shorthands = Shorthands()
		):
		super().__init__()
		self.operator = operator

		if operator == "LEAF":
			self.name = data["name"].lower()
			self.level = data["level"]
			self.tagged = data["tagged"]
			return

		for entry in data:
			if isinstance(entry, PreqNode):
				self.append(entry)
			elif entry["name"] in self._opperator_map.keys():
				self.append(PreqNode(entry["value"], entry["name"], skill))
			else:
				preqs = [{
					"name":name.lower(), 
					"level":entry["level"] if "level" in entry else "0", 
					"tagged":entry["tagged"] if "tagged" in entry else None
				} for name in sh.expand(entry["name"], skill)]
				preqs = [PreqNode(p, "LEAF", skill) for p in preqs]

				if len(preqs) == 1: self.append(preqs[0])
				else: self.append(PreqNode(preqs, "OR", skill))

	def at(self, level: int, tag: str =None) -> "PreqNode":
		"""Get a version of the tree from this node on as it would be at the given level

		Args:
			level (int): The level to evaluate the prerequisite at

		Returns:
			The prerequisite at the given level(, and with the given
			tag, if applicable)
		"""
		if self.operator == "LEAF":
			return PreqNode({
				"name":self.name,
				"level":str(eval(self.level, {"L":level})),
				"tagged":self.tagged and tag
			}, "LEAF")

		if len(self) == 0: return self
		return PreqNode((i.at(level, tag) for i in self), operator=self.operator)

	def satisfied_by(self, skills: "SkillList", tag: str =None) -> bool:
		"""Check whether the given SkillList satisfies this prerequisite tree

		A null node will always return true, a LEAF node will evaulate if any
		skill in the list matches the internal info, and any other node will
		check each child node, then tally them based on its operator.

		Args:
			skills (SkillList): The SkillList of skills to check against
			tag (str | None): The tag of the checked skill, in case a preq is tagged

		Returns:
			If the list satisfies the prerequisites or not

		Raises:
			AttributeError: If the level is not defined. The level may be defined 
				from an "at" call, or simply because it doesn't change with level
		"""
		if self.operator == "LEAF":
			try: int(self.level)
			except: raise AttributeError("cannot verify that prerequisite "
					"is met if the prerequisite level has not been defined")
			for skill in skills:
				ncheck  = skill.name.lower() == self.name

				lcheck  = str(skill.level) == self.level
				lcheck |= skill.level == 0
				lcheck |= self.level == "0"

				tcheck  = not self.tagged
				tcheck |= skill.tag == tag

				if ncheck and lcheck and tcheck:
					return True
			return False

		subtree = (child.satisfied_by(skills, tag) for child in self)
		# An empty subtree is truthy, with even parity (since 0 is even)
		if len(self) == 0: subtree = [True, True]
		return self._opperator_map[self.operator](subtree)

	@property
	def depth(self):
		if self.operator == "LEAF":
			return 1
		return max(i.depth for i in self) + 1

	def __repr__(self):
		s = f"PreqNode[{self.operator}]("
		if self.operator == "LEAF":
			s += self.name.title() + ", "
			s += self.level
			if self.tagged is not None:
				s += ", " + str(self.tagged)

		else: s += ', '.join(repr(n) for n in self)

		return s + ")"

	def __str__(self):
		if self.operator == "LEAF":
			s = f"{self.name.title()}"
			if self.level != "0":
				s += f" {self.level}"
			if self.tagged is not None:
				s += f" [{str(self.tagged)}]"
			return s

		if not self:
			return "None"

		if self.operator == "ROOT":
			if self.depth == 1:
				return ", ".join(str(n) for n in self)
			return ' AND '.join(str(n) for n in self)

		if self.operator in ("AND", "ALL"):
			return f"({' AND '.join(str(n) for n in self)})"

		if self.operator in ("OR", "ANY"):
			return f"({' OR '.join(str(n) for n in self)})"

		s  = f"[{self.operator}]("
		s += ", ".join(str(n) for n in self)
		s += ")"
		return s

	def __contains__(self, item):
		if self.operator == "LEAF":
			return item in self.name
		return any(item in sub for sub in self)

	def __len__(self):
		if self.operator == "LEAF":
			return 1
		elif self == []:
			return 0
		else:
			return sum(len(n) for n in self)

class Skill:
	"""A generalized or specific skill listing.

	Attributes:
		name (str): The name of the skill.
		levels (int): The number of levels the skill can have at most. A 0
			indicates that there is no limit on this skill's levels.
		cost_equation (str): The equation for the cost of this skill at a
			level, "l". This must be a valid python expression that 
			returns an integer, and only uses the variable "l".
		max_cost (int): The cap on the cost of any level of this skill.
			For example, if the cost is "l*5", but the max_cost is
			15, then the cost for each level will go 5, 10, 15, 15,
			15, 15, etc.
		codes (dict): The dictionary of codes for determining how a skill 
			can be learned. The keys may contain integers counting
			up from 1, and must contain a "+" entry for the code of
			any further levels.
		preqs (dict): This is a dictionary with the same key rules as
			the codes attribute. Its values are root nodes of trees of 
			prerequisites that are required to learn this skill.
		tags (list): The possible tags for this skill.
		category (str): The skill category.
		description (str): The skill description.
	"""
	def __init__(
			self, 
			data: Union[dict, "Skill"], 
			level: int = None, 
			tag: str = None,
			sh: Shorthands = Shorthands()
		):
		# General skill, produced from a dict as like in the skills config file
		if isinstance(data, dict) and level is None:
			self.__dict__.update(data)
			self._max_level = data["max_level"]
			self._max_cost = data["max_cost"]
			self.preqs = dict()
			for l, p in self.prerequisites.items():
				self.preqs[l] = PreqNode(p, "ROOT", self.name, sh)
			del self.__dict__["prerequisites"]
			tags_2d = (sh.expand(tag, self.name) for tag in self.tags)
			self.tags = [tag for sublist in tags_2d for tag in sublist]
			self.level = None

		# Specific skill, produced from an existing Skill
		elif isinstance(data, Skill) and level is not None:
			self.__dict__.update(data.__dict__)
			self.level = level
			if tag is not None and tag not in data.tags:
				raise ValueError(f"Tag name '{tag}' is not a valid option for this skill.")
			self.tag = tag

		# Specific skill, produced from a dict as like in the skills config file
		elif isinstance(data, dict) and level is not None:
			self.__dict__ = Skill(data, sh=sh).at(level, tag).__dict__

		# General skill, produced from an existing skill
		elif isinstance(data, Skill) and level is None:
			self.__dict__ = data.__dict__
			self.__dict__.pop("tag", None)
			self.level = None
	
	@property
	def max_level(self) -> Union[int, float]:
		if self._max_level == 0:
			return float("inf")
		return self._max_level

	@property
	def max_cost(self) -> Union[int, float]:
		if self._max_cost == 0:
			return float("inf")
		return self._max_cost

	@property
	def cost(self):
		if self.level is None:
			raise AttributeError("Only skills with a defined level have a cost")
		return self.cost_at(self.level)

	@property
	def code(self):
		if self.level is None:
			raise AttributeError("Only skills with a defined level have a code")
		return self.code_at(self.level)

	@property
	def preq(self):
		if self.level is None:
			raise AttributeError("Only skills with a defined level have a prerequisite")
		return self.preq_at(self.level)

	def cost_at(self, level: int) -> int:
		"""Gets the cost of the skill at the given level.

		Args:
			level (int): The level to get the cost for.

		Returns:
			The cost.
		"""
		actual = eval(self.cost_equation, {"L":level})
		return min(actual, self.max_cost)

	def code_at(self, level: int) -> str:
		"""Gets, at the given level, the code for determining how the skill can be learned.

		Args:
			level (int): The level to get the code for.

		Returns:
			The code.

		Raises:
			ValueError: If the level given is not a +ve int below a possible maximum.
		"""
		for l, code in self.codes.items():
			if l == str(level):
				return code

		if 0 < level <= self.max_level:
			return self.codes["+"]

		raise ValueError("The level to get a code must be a positive"
						 " number, up to the maximum allowed level.")

	def preq_at(self, level: int) -> PreqNode:
		"""Gets, at the given level, the prerequisites for learning the skill.

		Args:
			level (int): The level to get the prerequisite for.

		Returns:
			The root node of an updated prerequisite tree.

		Raises:
			ValueError: If the level given is not a +ve int below a possible maximum.
		"""
		tag = None
		if self.level is not None:
			tag = self.tag

		for l, preq in self.preqs.items():
			if l == str(level):
				return preq.at(level, tag)

		if 0 < level <= self.max_level:
			return self.preqs["+"].at(level, tag)

		raise ValueError("The level to get a prerequisite must be a positive"
						 " number, up to the maximum allowed level.")

	def max_display_level(self, limit: int =1000) -> int:
		"""Return the highest level that would have some unique value for an attribute.

		Args:
			limit (int): A cap on the return value, in case of infinities. defaults to 1000.
		
		Returns:
			The highest possible unique level, capped at "limit"
		"""
		# collect the levels at which there are unique codes and prereqs, 
		# and find the max of each (after replacing "+" with, essentially,
		# one more than the maximum otherwise)
		codes  = set(self.codes.keys()) - {"+"} | {"0"}
		prereq = set(self.preqs.keys()) - {"+"} | {"0"}
		codes, prereq = max(map(int, codes)) + 1, max(map(int, prereq)) + 1

		# find the level at which the next level would exceed the max cost
		l = 1
		if self.max_cost != float("inf"):
			while self.cost_at(l+1) < self.max_cost and l <= limit:
				l += 1

		result = max(codes, prereq, l, self.max_level)
		if result == float("inf"):
			result = max(codes, prereq, l)
		return min(result, limit)

	def at(self, level: int, tag: str =None) -> "Skill":
		"""Get a version of this skill at the given level (optionally with the given tag).
	
		Args:
			level (int): The level to get the skill at.
			tag (str | None): The tag to put on the returned skill. Defaults to None.

		Returns:
			A Skill based on this skill, but at the given level.
		"""
		return Skill(self, level, tag)

	def general(self) -> "Skill":
		"""Get a general version of this skill"""
		return Skill(self)

	def satisfied_by(self, skills: "SkillList") -> bool:
		"""Checks whether this skill's prerequisites are met by the given SkillList

		This method only works for a skill that has been defined at some level.

		Args:
			skills: The SkillList of Skills to check against

		Returns:
			If the list satisfies the prerequisites or not

		Raises:
			AttributeError: If all of this skill's prerequisites aren't 
				well defined, either from being retrieved from an "at" 
				method call, or because they don't change with the level
		"""
		return self.preq.satisfied_by(skills, self.tag)

	def __repr__(self):
		s = f"Skill[{'General' if self.level is None else'Defined'}]("
		s += self.name
		if self.level is not None:
			s += f", {self.level}"
			if hasattr(self, "tag") \
			and self.tag is not None:
				s += f", {self.tag}"
		return s + ")"

	def __str__(self):
		s = self.name
		if self.level is not None:
			s += f" {self.level}"
			if self.tag is not None:
				s += f" [{self.tag}]"
		return s

class Spell:
	"""A class that holds information on a spell"""
	def __init__(self, data, **kwargs):
		for attr in ("name", "tier", "element", "description", "effect"):
			value = kwargs.get(attr, data.get(attr, None))

			if value is None:
				n = "n" if attr[0] in "aeiou" else ""
				raise ValueError(f"Spell requires a{n} {attr}")

			if attr is "tier":
				try: value = int(value)
				except ValueError:
					raise ValueError("Spell tier must be castable to int")

			self.__dict__[attr] = value

		self.timed = kwargs.get("timed", data.get("timed", False))

	def __repr__(self):
		return f"Spell({self.name})"

	def __str__(self):
		return self.name

class Weapon:
	"""A container for a weapon and its attributes"""
	def __init__(
		self, 
		name: str, 
		damage: int, 
		difficulty: str,
		two_handed: bool, 
		boffer_arrow: bool,
		tip_only: bool
	):
		for k, v in locals().items():
			if k == "self": continue
			self.__dict__[k] = v

	def __repr__(self):
		s = "Weapon("
		s += f"{self.name}, "
		s += f"{self.damage}, "
		s += f"{self.difficulty})"
		return s

	def __str__(self):
		return self.name

class Rulebook:
	"""A class to hold all rulebook information for browsing"""
	def __init__(self, data):
		self.sections = []
		for title, section in data.items():
			section = RulesSection(title, section, self)
			self.sections.append(section)

	@property
	def path(self):
		return "Rulebook"

	def __str__(self):
		return "Rulebook"

class RulesSection:
	"""A class to store a section of the rulebook"""
	def __init__(self, title, data, parent):
		self.title = title
		self.parent = parent
		self.terms, self.topics = [], []

		for title, text in data["Terms"].items():
			page = RulesPage(title, text, self)
			self.terms.append(page)

		for title, topic in data["Topics"].items():
			topic = RulesTopic(title, topic, self)
			self.topics.append(topic)

	@property
	def path(self):
		return f"{self.parent.path} / {self.title}"

	def __str__(self):
		return self.title.title()

class RulesTopic:
	"""A class to store a topic in a rules section"""
	def __init__(self, title, data, parent):
		self.title = title
		self.parent = parent
		self.pages = []
		for title, text in data.items():
			page = RulesPage(title, text, self)
			self.pages.append(page)

	@property
	def path(self) -> str:
		return f"{self.parent.path} / {self.title}"

	def __str__(self):
		return self.title.title()

class RulesPage:
	"""A class to store a page in a rules topic/section"""
	def __init__(self, title, text, parent):
		self.title = title
		self.text = text
		self.parent = parent

	@property
	def path(self) -> str:
		return f"{self.parent.path} / {self.title}"
		
	def __str__(self):
		return self.title.title()

class InvalidFieldError(ValueError): pass
class SearchError(Exception): pass
