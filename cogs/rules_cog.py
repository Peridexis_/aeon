from discord.ext import commands as cmds
from rules import find, get_fields
from embeds import SkillSearchMsg, SpellSearchMsg, WeaponSearchMsg, RulebookMsg
from common import SearchError, InvalidFieldError
from storage import get_search_keywords, get_rulebook

class Rules(cmds.Cog):
	def __init__(self, bot):
		self.bot = bot
		self.comparisons = {
			"<" : int.__lt__,
			"<=": int.__le__,
			">" : int.__gt__,
			">=": int.__ge__,
			"=" : int.__eq__,
			"==": int.__eq__
		}
		self.err = {
			"by without field":'A "by", "in", or "with" keyword must be followed by the name of the field being searched by.',
			"no ____s found":lambda t: f"There were no {t}s found that match",
			"too many ____ comps":lambda t: f"When searching by {t}, only specify one of {', '.join(self.comparisons.keys())}",
			"non-numeric":lambda t: f"When searching by {t}, the search term must be a number"
		}
		self.msgs = {
			"skill": SkillSearchMsg,
			"spell": SpellSearchMsg,
			"weapon": WeaponSearchMsg
		}
		self.numeric_fields = {"skill":["cost", "reward"], "spell":["tier"], "weapon":["damage"]}
		self.boolean_fields = {"weapon":["two handed", "boffer arrow", "tip only"]}

	@cmds.group(invoke_without_command=True)
	async def search(self, ctx):
		pass # super-command for searching the rulebook

	def _parse_search(self, type, data):
		kws = get_search_keywords()
		numerics = self.numeric_fields
		booleans = self.boolean_fields
		splt = list(map(str.lower, data))
		data = dict()
		data["negate"] = False

		for n in kws["Negatives"]:
			for _ in range(splt.count(n)):
				data["negate"] ^= True # flip
				splt.remove(n)

		for i in kws["Ignore"]:
			for _ in range(splt.count(i)):
				splt.remove(i)

		field = "name"
		for i in range(len(splt)):
			if splt[i] in kws["All Flags"]:
				try: field = splt[i+1]
				except IndexError:
					raise SearchError(self.err["by without field"])

				data["negate"] ^= splt[i] in kws["Negative Flags"] # flip if
				del splt[i+1]
				del splt[i]
				break
		else:
			for i in splt:
				if any(i in b for b in booleans.get(type, ""))\
				or any(i in n for n in numerics.get(type, "")):
					splt.remove(i)
					field = i
					break

		fields = get_fields(type, field)
		if len(fields) < 1: raise InvalidFieldError(f"\"{field}\" matches no valid search fields")
		if len(fields) > 1: raise InvalidFieldError(f"\"{field}\" could be multiple search fields")
		data["field"] = fields[0]

		data["comp"] = None
		data["search"] = ' '.join(splt)
		if data["field"] in numerics.get(type, ""):
			common = set(self.comparisons.keys()) & set(splt)
			if len(common) > 1:
				raise SearchError(self.err["too many ____ comps"](data["field"]))
			elif len(common) == 1:
				data["comp"] = common.pop()
				splt.remove(data["comp"])
				data["search"] = ' '.join(splt)

			try: data["search"] = int(data["search"])
			except ValueError:
				raise SearchError(self._err["non-numeric"](data["field"]))

		if data["field"] in booleans.get(type, ""):
			data["search"] = not data["negate"]
			data["negate"] = False

		return data

	async def _do_search(self, type, ctx, data):
		try:
			data = self._parse_search(type, data)
			compare = self.comparisons.get(data.get("comp"))
			search, field = data["search"], data["field"]

			items = find(type, search, field, compare=compare, negate=data["negate"])
			if not items:
				raise SearchError(self.err["no ____s found"](type))

			await self.msgs[type](items, search, field, data.get("comp")).send_to(ctx)

		except (SearchError, InvalidFieldError) as e:
			await ctx.send(e)

	@search.command(aliases=["skill"])
	async def skills(self, ctx, *data):
		await self._do_search("skill", ctx, data)

	@search.command(aliases=["spell"])
	async def spells(self, ctx, *data):
		await self._do_search("spell", ctx, data)

	@search.command(aliases=["weapons"])
	async def weapon(self, ctx, *data):
		await self._do_search("weapon", ctx, data)

	@cmds.command()
	async def rules(self, ctx):
		await RulebookMsg(get_rulebook()).send_to(ctx)
