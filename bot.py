from discord.ext import commands as cmds
from embeds import action

aeon = cmds.Bot(command_prefix="!")
cogs = ["rules"]

@aeon.command(aliases=["quit", "exit"])
async def stop(ctx):
	await aeon.close()

@aeon.event
async def on_reaction_add(reaction, user):
	if user.id != aeon.user.id:
		await action(reaction, user)

if __name__ == '__main__':
	with open("configs/token.txt") as file:
		token = file.readline()
	for cog in cogs:
		exec(f"from cogs.{cog}_cog import {cog.capitalize()} as c")
		aeon.add_cog(c(aeon))
	aeon.run(token)