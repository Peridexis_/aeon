from storage import get_general_skill_list, get_skill_list, get_spell_list, get_weapon_list
from typing import Union, Callable

def _get_checks(
		type: str,
		search: Union[int, str, bool] = None,
		compare: Callable[[int, int], int] = None
	) -> dict:
	if search is None: search = ""
	if compare is None: compare = int.__eq__
	if type == "skill":
		g, d = dict(), dict() # for partial checks for general and defined skills
		g["cost"] = lambda s: search in s.cost_equation
		d["cost"] = lambda s: s.cost >= 0 and compare(s.cost, int(search))
		g["rewd"] = lambda s: search in s.cost_equation
		d["rewd"] = lambda s: s.cost <= 0 and compare(-s.cost, int(search))
		g["code"] = lambda s: any(search in c.lower() for c in s.codes.values())
		d["code"] = lambda s: search in s.code.lower()
		g["preq"] = lambda s: any(search in p for p in s.preqs.values())
		d["preq"] = lambda s: search in s.preq
		g["tag"]  = lambda s: any(search in t.lower() for t in s.tags)
		d["tag"]  = lambda s: bool(s.tag) and search in s.tag.lower()

		return {
			"name":			lambda s: search in s.name.lower(),
			"level":		lambda s: search == str(s.level),
			"cost":			lambda s: (g if s.level is None else d)["cost"](s),
			"reward":		lambda s: (g if s.level is None else d)["rewd"](s),
			"code":			lambda s: (g if s.level is None else d)["code"](s),
			"prerequisite":	lambda s: (g if s.level is None else d)["preq"](s),
			"tag":			lambda s: (g if s.level is None else d)["tag"](s),
			"category":		lambda s: search in s.category.lower(),
			"description":	lambda s: search in s.description.lower()
		}
	elif type == "spell":
		return {
			"name":			lambda s: search in s.name.lower(), 
			"tier":			lambda s: search in s.tier.lower(), 
			"element":		lambda s: search in s.element.lower(), 
			"description":	lambda s: search in s.description.lower(), 
			"effect":		lambda s: search in s.effect.lower(),
			"tier":			lambda s: compare(s.tier, int(search))
		}
	elif type == "weapon":
		def _dmg_check(w):
			result = compare(w.damage, int(search))
			if w.boffer_arrow:
				result |= compare(w.damage+2, int(search))
			return result

		return {
			"name":			lambda w: search in w.name,
			"damage":		_dmg_check,
			"difficulty":	lambda w: search in w.difficulty,
			"two handed":	lambda w: bool(search) == w.two_handed,
			"boffer arrow": lambda w: bool(search) == w.boffer_arrow,
			"tip only":		lambda w: bool(search) == w.tip_only
		}

def get_fields(type: str, filter: str = None):
	"""Get a list of all possible searchable fields for `type` (filtered by `filter` if given)."""
	if filter is None: filter = ""
	return [i for i in _get_checks(type).keys() if filter in i]

def find(
		type: str,
		search: Union[int, str, bool],
		field: str = "name",
		*,
		compare: Callable[[int, int], int] = None,
		negate: bool = False,
		general: bool = False
	) -> list:
	""" Find all items of type `type` matching the given term in the given field.

	If the field is numeric, use compare to set the function used to compare the costs with the search 
	term. This allows finding all items with some attribute <=, >, etc. a value, i.e. all skills tier >= 3.

	Args:
		search (Union[int, str]): The term to search by. int if field is tier, str otherwise
		field (str): The field to be searching in. Assumed valid field for `type`
		compare (Callable): If field is tier, this is the comparison operator used. defaults to int.__eq__
		general (bool): If the type is skill, determines if the results of general or specific skills are used
	
	Returns:
		A list of all matching items
	
	Raises:
		TypeError: If the type doesn't match any known type
	"""
	if isinstance(search, str):
		search = search.lower()

	lists = {
		"spell": get_spell_list,
		"skill": get_general_skill_list if general else get_skill_list,
		"weapon": get_weapon_list
	}

	if type not in lists:
		raise TypeError(f"Type must be one of {', '.join(lists.keys())}")

	checks = _get_checks(type, search, compare)
	return [i for i in lists[type]() if negate ^ checks[field](i)]
