from typing import Union, Sequence
from collections import OrderedDict
import functools
import shelve
import json
from common import Shorthands, Skill, Spell, Weapon, Rulebook

def refresh_configs():
	"""Refresh the internal _configs dict with the config files."""
	global _configs
	_configs = dict()
	for name in ("misc", "psionics", "races", "skills", "spells", "weapons", "rulebook"):
		with open(f"configs/{name}.json", "r") as file:
			_configs[name] = json.load(file)

	_configs["weapon skills"] = _configs["weapons"]["Weapon Skills"]
	twohand     = _configs["weapons"]["Two Handed"]
	boffarro    = _configs["weapons"]["Boffer Arrow +2 Damage"]
	tiponly     = _configs["weapons"]["Tip Only Damage"]
	raw_weapons = _configs["weapons"]["Weapons"]
	_configs["weapons"] = []
	for diff, weapons in raw_weapons.items():
		for name, dmg in weapons.items():
			_configs["weapons"].append(Weapon(
				name = name,
				damage = dmg,
				difficulty = diff,
				two_handed = name in twohand, 
				boffer_arrow = name in boffarro,
				tip_only = name in tiponly
			))

	kws = _configs["misc"]["Search Keywords"].copy()
	all_flags = kws["Field Flags"] + kws["Negative Flags"]
	_configs["misc"]["Search Keywords"]["All Flags"] = all_flags

	sh = get_shorthands()
	_configs["skills"] = [Skill(s, sh=sh) for s in _configs["skills"]]
	_configs["spells"] = [Spell(s) for s in _configs["spells"]]
	_configs["rulebook"] = Rulebook(_configs["rulebook"])

def _uses_configs(func):
	@functools.wraps(func)
	def wrapper(*args, **kwargs):
		global _configs
		if "_configs" not in globals():
			refresh_configs()
		return func(*args, **kwargs)
	return wrapper

@_uses_configs
def get_shorthands() -> Shorthands:
	"""Gets a Shorthands object to use to expand names of prerequisites and tags."""
	sh = _configs["misc"]["Shorthands"].copy()

	sh["WEAPON"] = []
	for weapon in _configs["weapons"]:
		sh["WEAPON"].append(weapon.name)

		diff = weapon.difficulty.upper()
		if diff not in sh: sh[diff] = []
		sh[diff].append(weapon.name)

	sh["WSKILL"] = _configs["weapon skills"][:]

	sh["PRIMEELEMENT"] = _configs["misc"]["Prime Elements"][:]
	sh["PUREELEMENT"]  = _configs["misc"]["Pure Elements"][:]
	sh["ANYELEMENT"]   = sh["PUREELEMENT"] + sh["PRIMEELEMENT"]

	return Shorthands(sh)

@_uses_configs
def get_general_skill_list() -> list:
	"""Gets a list of general raw skill as defined in the config file

	Returns:
		A list of skills from the skills.json config
		file, packaged into Skill objects
	"""
	return _configs["skills"]

def get_skill_list() -> list:
	"""Get a list of all possible level and tag combos for skills

	Returns:	
		A list of Skills for all possible levels and tags
	"""
	result = []
	for gskill in get_general_skill_list():
		for level in range(1, gskill.max_display_level() + 1):
			for tag in gskill.tags or (None,):
				result.append(gskill.at(level, tag))
	return result

@_uses_configs
def get_code_meaning(code: str) -> OrderedDict:
	"""Get the meaning of the given skill code/codes.

	Args:
		code (str): The skill code(s) in question

	Returns:
		The full meaning(s) of the code(s) in configs
	"""
	codes = _configs["misc"]["Code Meanings"]
	values = (codes.get(c, "(unknown code)") for c in code)
	return OrderedDict(zip(code, values))

@_uses_configs
def get_spell_list() -> list:
	"""Gets a list of all spells from the configs

	Returns:
		A list of Spell objects
	"""
	return _configs["spells"][:]

@_uses_configs
def get_weapon_list() -> list:
	"""Gets a list of all weapons from the configs

	Returns:
		A list of Weapon objects
	"""
	return _configs["weapons"][:]

@_uses_configs
def get_search_keywords() -> dict:
	"""Gets a dict of all search keywords"""
	return _configs["misc"]["Search Keywords"].copy()

@_uses_configs
def get_message_color(type: str) -> str:
	"""Gets the name of the color for a given message type"""
	return _configs["misc"]["Message Colors"][type]

@_uses_configs
def get_rulebook() -> dict:
	"""Gets the dictionary of rules"""
	return _configs["rulebook"]

def _uses_rmsgs(func):
	@functools.wraps(func)
	def wrapper(*args, **kwargs):
		global _rmsgs
		if "_rmsgs" not in globals():
			_rmsgs = dict()
		return func(*args, **kwargs)
	return wrapper

@_uses_rmsgs
def store_reactive_msg(rmsg: "ReactiveMsg"):
	if rmsg.id is None:
		raise ValueError("Cannot store a ReactiveMsg without a set id")
	_rmsgs[rmsg.id] = rmsg

@_uses_rmsgs
def get_reactive_msg(mid: int):
	if mid not in _rmsgs:
		return None
	return _rmsgs[mid]

@_uses_rmsgs
def delete_reactive_msg(mid: int):
	del _rmsgs[mid]

if __name__ == '__main__':
	print(get_weapon_list())