from embeds.base import ReactiveMsg, ListMsg
from common import Rulebook, RulesSection, RulesTopic, RulesPage

class RulebookMsg(ListMsg):
	def __init__(self, rulebook, *args, **kwargs):
		super().__init__(rulebook.sections, *args, **kwargs)
		self._files = ["qmark.png"] # "↩️" "➖" "➕"
		self._view = rulebook
		self._type = "rulebook"
		
		for s, e in zip(self._items, self._nums):
			self._actions[e] = self._section(s)

	@property
	def embed(self):
		name = self._view.path
		if self._pmax > 1:
			name += f" - Page {self._page+1}"
		em = super().embed.set_author(
			name = name,
			icon_url = "attachment://qmark.png"
		)

		if isinstance(self._view, Rulebook):
			return em

		if isinstance(self._view, RulesSection):
			em.description = None
			return em.add_field(
				name = "1⃣ - Topics",
				value = "\n".join(map(str, self._view.topics))
			).add_field(
				name = "2⃣ - Terms",
				value = "\n".join(map(str, self._view.terms))
			).set_footer(
				text = "❌ to delete."
			)

	def _section(self, sec):
		async def _func(msg):
			self._view = sec
			await msg.edit(embed = self.embed)
			return True
		return _func