from storage import get_code_meaning, store_reactive_msg, delete_reactive_msg, get_reactive_msg, get_message_color
from discord import Embed, File, Color, Message, User, Reaction, Member, User
from discord.ext.commands import Context
from discord.errors import NotFound
from collections import OrderedDict
from typing import Union, Tuple
from asyncio import Lock

async def action(reaction: Reaction, user: Union[Member, User]) -> bool:
		"""Removes the emoji, then takes the appropriate action.

		This will remove any emoji passed to it from the given message,
		then will check if its internal dict of actions has any with the
		emoji as a key, and will take that action.

		Args:
			reaction (Reaction): The reaction being enacted

		Returns:
			bool: False if the ReactiveMsg wasn't found, True otherwise
		"""
		rmsg = get_reactive_msg(reaction.message.id)
		if rmsg is None: return False
		await rmsg.action(reaction, user)
		return True


class ReactiveMsg:
	"""An object to format the support special messages.

	A reactive message will be some message consisting primarily
	of an embed, and will have only certain preset reactions on
	it put there by the bot. Any other reactions will be removed,
	and if it's another of the special ones, some action will be
	taken.

	Attributes:
		id (int): The id of the discord message being manupulated
		content (str): The content of the non-embed part of the message
		embed (Embed): The embed of the message, re-generated each fetch
		files (list): The list of File objects to send for embed attachements
	"""
	_colors = {
		"purple": Color.from_rgb(85, 0, 85),
		"blue": Color.from_rgb(0, 0, 255),
		"green": Color.from_rgb(0, 170, 0),
		"red": Color.from_rgb(170, 0, 0),
		"orange": Color.from_rgb(255, 136, 0)
	}
	def __init__(self, content=None, *, id=None):
		self._action_lock = Lock()
		self._react_lock = Lock()
		self.id = id
		self.content = content
		self._actions = OrderedDict()
		self._actions["❌"] = self._delete
		self._files = []
		self._type = None

	@property
	def embed(self):
		"""The message embed, as it currently stands"""
		cname = get_message_color(self._type or "other")
		em = Embed(color=self._colors[cname])
		em.set_footer(text="❌ to delete.")
		return em

	@property
	def files(self):
		"""The list of Files to send for embed attachments"""
		cname = get_message_color(self._type or "other")
		path = f"icons/{cname}/"
		return [File(f"{path}{fn}", fn) for fn in self._files]

	async def send_to(self, ctx: Context) -> Message:
		"""Sends and returns the message, and sets the id.

		This will send the internal message to the context of the
		requesting command, then will set its internal id to that
		of the newly created message. It will return that message.

		Args:
			ctx (Context): The context of the message requesting this

		Returns:
			The message that was sent
		"""
		msg = await ctx.send(
			files = self.files, 
			embed = self.embed,
			content = self.content
		)
		self.id = msg.id
		store_reactive_msg(self)
		await self._refresh_reacts(msg)
		return msg

	async def action(self, reaction: Reaction, user: Union[Member, User]):
		"""Removes the emoji, then takes the appropriate action.

		This will remove any emoji passed to it from the given message,
		then will check if its internal dict of actions has any with the
		emoji as a key, and will take that action.

		Args:
			reaction (Reaction): The reaction being enacted
		"""
		emoji, msg = reaction.emoji, reaction.message
		await msg.remove_reaction(emoji, user)

		async with self._action_lock:
			if emoji not in self._actions: return
			ret = await self._actions[emoji](msg)

		# a return of "delete" is a request to delete
		if ret is "delete":
			delete_reactive_msg(self.id)
		# no (/a None) return value marks for re-storage
		elif ret is None:
			store_reactive_msg(self)
		# otherwise no action is taken

		await self._refresh_reacts(msg)

	async def _refresh_reacts(self, msg: Message):
		"""Resets the reactions on a message to match own actionable options.

		Args:
			msg (Message): The message on which to reset reactions

		Raises:
			ValueError: If another message is passed besides own one
		"""
		if self.id != msg.id:
			raise ValueError("Can only reset reactions on own message")

		async with self._react_lock:
			async with self._action_lock:
				emojis = iter(list(self._actions.keys()))
			reacts = msg.reactions[:]
			try: emoji = next(emojis)
			except StopIteration: emoji = None

			for r in reacts:
				if emoji == r.emoji:
					try: emoji = next(emojis)
					except StopIteration: emoji = None
					continue

				async for user in r.users():
					try: await msg.remove_reaction(r.emoji, user)

					except NotFound as e:
						if "Unknown Message" in str(e): return
						raise

			if emoji is not None:
				emojis = [emoji, *emojis]
			for emoji in emojis:
				try: await msg.add_reaction(emoji)

				except NotFound as e:
					if "Unknown Message" in str(e): return
					raise

	async def _delete(self, msg):
		await msg.delete()
		return "delete"

class ListMsg(ReactiveMsg):
	"""A subclass of ReactiveMsg to display a list of items."""
	def __init__(self, items, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._files = ["list.png"]
		self._items = items
		self._index = None
		self._page = 0
		self._plen = 9
		self._pmax = -(-len(items)//self._plen) # -(-n//d) == ceil(n/d)
		self._nums = ("1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣")
		# self._nums = self._nums[:self._plen]

		if self._pmax > 1:
			self._actions["➖"] = self._less

		p1len = min(len(items), self._plen)
		for n, e in enumerate(self._nums[:p1len]):
			self._actions[e] = self._number(n)

		if self._pmax > 1:
			self._actions["➕"] = self._more

	@property
	def embed(self):
		if self._index is None:
			return self._multiple(super().embed)
		return self._single(super().embed)

	def _single(self, embed):
		return embed.set_author(
			name = str(self._items[self._index]),
			icon_url = "attachment://bullet.png"
		)

	def _multiple(self, embed):
		name = "List"
		if self._type is not None:
			name = f"{self._type.title()} List"
		if self._pmax > 1:
			name += f" - Page {self._page+1}"

		embed.set_author(
			name = name,
			icon_url = "attachment://list.png"
		)

		pstart = self._page * self._plen
		pend = pstart + self._plen
		zipitems = zip(self._nums, self._items[pstart:pend])
		listitems = [f"{i} - {v}" for i, v in zipitems]
		embed.description = "\n".join(listitems)

		footer = "❌ to delete."
		if self._pmax > 1:
			footer = f"➖ for prev page. {footer} ➕ for next page."

		return embed.set_footer(text=footer)

	def _number(self, num):
		async def _func(msg):
			for e in self._nums:
				self._actions.pop(e, None)

			self._actions["↩"] = self._back
			if len(self._items) > 1:
				self._actions["➖"] = self._less
				self._actions.move_to_end("↩")
				self._actions["➕"] = self._more
				self._actions.move_to_end("➕")

			self._index = num
			await msg.edit(embed=self.embed)
		return _func

	async def _refresh_list(self, msg):
		if self._index is None:
			pstart = self._page*self._plen
			for n, e in enumerate(self._nums, pstart):
				if n < len(self._items):
					self._actions[e] = self._number(n)
				else:
					self._actions.pop(e, None)

			if self._pmax > 1:
				self._actions.move_to_end("➕")

		await msg.edit(embed=self.embed)

	async def _more(self, msg):
		if self._index is None:
			self._page = (self._page + 1) % self._pmax
		else:
			self._index = (self._index + 1) % len(self._items)
			self._page = self._index//self._plen

		return await self._refresh_list(msg)


	async def _less(self, msg):
		if self._index is None:
			self._page = (self._page - 1) % self._pmax
		else:
			self._index = (self._index - 1) % len(self._items)
			self._page = self._index//self._plen
		
		return await self._refresh_list(msg)

	async def _back(self, msg):
		self._index = None
		self._actions.pop("↩", None)
		if self._pmax == 1:
			self._actions.pop("➖", None)
			self._actions.pop("➕", None)
		return await self._refresh_list(msg)

	def __iter__(self):
		return iter(self._items)

class SearchMsg(ListMsg):
	def __init__(self, items, search, field="name", compare=None, *args, **kwargs):
		super().__init__(items, *args, **kwargs)
		self._files = ["qmark.png"]
		self._search = search
		self._field = field
		self._compare = compare or "=="

	@property
	def embed(self):
		em = super().embed
		return em.set_author(
			name = em.author.name,
			icon_url = "attachment://qmark.png"
		)