from embeds.base import ReactiveMsg, ListMsg, SearchMsg
from storage import get_code_meaning
from collections import OrderedDict

class SkillMsg(ReactiveMsg):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._type = "skill"

class SkillListMsg(SkillMsg, ListMsg):
	"""A subclass of ListMsg for skills."""
	def _single(self, embed):
		s = self._items[self._index]
		fields = OrderedDict()
		if s.level is None:
			codes = sorted(s.codes.items())
			codes.append(codes.pop(0))
			codes = (f"{l}: {c}" for l, c in codes)

			preqs = sorted(s.preqs.items())
			preqs.append(preqs.pop(0))
			preqs = (f"{l}: {c}" for l, c in preqs)

			cname = "Cost"
			if "L" in s.cost_equation:
				cname += " At Level \"L\""

			fields[cname] = s.cost_equation
			fields["Skill Code Per Level"] = "\n".join(codes)
			fields["Prerequisites per level"] = "\n".join(preqs)
			fields["Category"] = s.category
			if s.max_level != float("inf"):
				fields["Max Level"] = s.max_level
			if s.max_cost != 0:
				fields["Cost Cap"] = s.max_cost
			if s.tags:
				fields["Tag Options"] = ", ".join(s.tags)

			name = s.name
		else:
			meanings = get_code_meaning(s.code).items()
			codes = (f"{c} - {m}" for c, m in meanings)

			pname, cname = "Prerequisite", "Skill Code"
			if len(s.preq) != 1: pname += "s"
			if len(meanings) != 1: cname += "s"

			fields["Skill Cost"] = f"{s.cost} XP"
			fields[cname] = "\n".join(codes)
			fields[pname] = str(s.preq)

			name = f"{s.name} {s.level}"
			if s.tag is not None:
				name += f" [{s.tag}]"

		embed.set_author(
			name = name,
			icon_url = "attachment://bullet.png"
		)

		for n, v in fields.items():
			embed.add_field(name=n, value=v)

		return embed

class SkillSearchMsg(SearchMsg, SkillListMsg):
	"""A subclass of SkillListMsg for lists of skill search results"""
	def _multiple(self, embed):
		if self._field in ("cost", "reward"):
			name = f"Skills with {self._field} {self._compare} {self._search}"
		else:
			name = f"Skills containing \"{self._search}\""
			if self._field != "name":
				name += f" in {self._field}"

		if self._pmax > 1:
			name += f" - Page {self._page+1}"

		return super()._multiple(embed).set_author(
			name = name,
			icon_url = "attachment://qmark.png"
		)
