from embeds.base import ReactiveMsg, ListMsg, SearchMsg

class SpellMsg(ReactiveMsg):
	"""A subclass of a ReactiveMsg to set attributes for spells"""
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._type = "spell"

class SpellListMsg(SpellMsg, ListMsg):
	"""A subclass of ListMsg for spells."""
	def _single(self, embed):
		s = self._items[self._index]
		th = {1:"st", 2:"nd", 3:"rd"}.get(s.tier%10, "th")
		if s.tier in (11, 12, 13): th = "th"

		return embed.set_author(
			name = s.name,
			icon_url = "attachment://bullet.png",
		).add_field(
			name = "Tier",
			value = f"{s.tier}{th} level spell"
		).add_field(
			name = "Element",
			value = s.element
		).add_field(
			name = "Description",
			value = s.description
		).add_field(
			name = "In Game Effect",
			value = s.effect
		)

class SpellSearchMsg(SearchMsg, SpellListMsg):
	"""A subclass of SpellListMsg for lists of spell search results"""
	def _multiple(self, embed):
		if self._field == "tier":
			name = f"Spells with tier {self._compare} {self._search}"
		else:
			name = f"Spells containing \"{self._search}\""
			if self._field != "name":
				name += f" in {self._field}"

		if self._pmax > 1:
			name += f" - Page {self._page+1}"

		return super()._multiple(embed).set_author(
			name = name,
			icon_url = "attachment://qmark.png"
		)
