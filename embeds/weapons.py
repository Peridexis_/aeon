from embeds.base import ReactiveMsg, ListMsg, SearchMsg
from discord import Embed

class WeaponMsg(ReactiveMsg):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._type = "weapon"

class WeaponListMsg(WeaponMsg, ListMsg):
	"""A subclass of ListMsg for spells."""
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		for n in self._nums:
			self._actions.pop(n, None)
		self._plen = 5
		self._nums = tuple()

	def _multiple(self, embed):
		embed = super()._multiple(embed)
		embed.description = None

		pstart = self._page*self._plen
		pend = pstart+self._plen
		for weapon in self._items[pstart:pend]:
			name = f"{weapon.name} [{weapon.difficulty}]"
			value = f"Deals {weapon.damage} damage"
			if weapon.two_handed:
				value += "\nMust be used two-handed"
			if weapon.boffer_arrow:
				value += f"\nDeals {weapon.damage+2} damage when using boffer arrows"
			if weapon.tip_only:
				value += "\nDamages with the tip only"
			embed.add_field(name=name, value=value, inline=False)
		return embed

class WeaponSearchMsg(SearchMsg, WeaponListMsg):
	"""A subclass of SpellListMsg for lists of spell search results"""
	def _multiple(self, embed):
		dont = "" if self._search else " don't"
		name = {
			"name": f"Weapons matching \"{self._search}\"",
			"difficulty": f"Weapons with difficulty matching \"{self._search}\"",
			"damage": f"Weapons with damage {self._compare} {self._search}",
			"boffer arrow": f"Weapons which{dont} gain a bonus +2 damage for using boffer arrows",
			"two handed": f"Weapons which{dont} need to be used two handed",
			"tip only": f"Weapons which{dont} deal damage exclusively with their tip"
		}[self._field]

		if self._pmax > 1:
			name += f" - Page {self._page+1}"

		return super()._multiple(embed).set_author(
			name = name,
			icon_url = "attachment://qmark.png"
		)
